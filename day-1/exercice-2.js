import { my_display_alpha } from './exercice-1.js';
import { my_size_alpha } from './exercice-4.js';

export function my_display_alpha_reverse() {
    const alpha = my_display_alpha();
    let len = my_size_alpha(alpha) - 1;
    let alphaResverse = '';
    let count = 0;

    while(!!alpha[count]) {
        alphaResverse += alpha[len];

        len--;
        count++;
    }

    return alphaResverse;
}