export function my_is_posi_neg(nbr) {

    let retour;

    if(nbr < 0){
        return "NEGATIF";
    }
    else if(nbr === 0){
        return "NEUTRAL";
    }
    
    return "POSITIF";

}
