export function my_string_is_number(str = '') {

    if(typeof str == "string") {

        let count = 0;

        const nombres = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];

        while(!!str[count]){
            for(let i = 0; i < 10; i++) {
                if(str[count] == nombres[i]) {
                    return true;
                }
            }
            count++;
        }

    }
    
    return false;

}