export function my_alpha_reverse(str = '') {

    let alphaResverse = '';

    if(typeof str == "string") {
        let count = 0;
        while(!!str[count]){
            count++;
        }

        let len = count - 1;
        let i = 0;

        while(!!str[i]) {
            alphaResverse += str[len];

            len--;
            i++;
        }
    }

    return alphaResverse;
    
}