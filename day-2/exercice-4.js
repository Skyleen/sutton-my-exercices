export function my_display_comb() {

    let comb = [];
    let count = 0;

    for (let i = 0; i <= 2; i++)
    {
        for (let j = 0; j <= 99; j++)
        {
            comb[count] = [`${i} ${j}`];
            count++;
        }
    }

    return comb;
}