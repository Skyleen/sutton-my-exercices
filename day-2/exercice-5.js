export function my_display_combv2() {
    let comb = [];
    let count = 0;

    for (let i = 0; i <= 9; i++)
    {
        for (let j = 0; j <= 9; j++)
        {
            for (let k = 0; k <= 9; k++)
            {
                if(i != j && i != k && j != k){
                    comb[count] = `${i} ${j} ${k}`;
                    count++;
                }
                if(i == 7 && j == 8 && k == 9){
                    return comb;
                }
            }
        }
    }
}